package com.maple.el.manage;

import com.maple.el.entity.Scene;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author: maple
 * @version: SceneMap.java, v 0.1 2020年09月15日 15:43 maple Exp $
 */
public class SceneCache {

    private static ConcurrentMap<String, Scene> sceneMap = new ConcurrentHashMap<>();

    /**
     * 把场景缓存一下
     *
     * @param sceneList
     */
    public static void put2map(List<Scene> sceneList) {

        if (!sceneMap.isEmpty()) {
            sceneMap.clear();
        }

        for (Scene scene : sceneList) {
            String expression = scene.getExpression();
            if (!sceneMap.containsKey(expression)) {
                sceneMap.put(expression, scene);
            } else {
                System.out.println("found duplication expression");
            }
        }
    }

    /**
     * 获取所有场景
     *
     * @return
     */
    public static Map<String, Scene> getAll() {
        return sceneMap;
    }
}
