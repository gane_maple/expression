package com.maple.el.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maple.el.entity.RuleEntity;
import com.maple.el.entity.Scene;
import com.maple.el.entity.SceneRuleRel;
import com.maple.el.service.RuleService;
import com.maple.el.service.SceneRuleRelService;
import com.maple.el.service.SceneService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author: maple
 * @version: AbstractAuthManage.java, v 0.1 2020年09月15日 15:23 maple Exp $
 */
@Configuration
public class AbstractAuthManage implements InitializingBean {

    @Resource
    private RuleService ruleService;

    @Resource
    private SceneService sceneService;

    @Resource
    private SceneRuleRelService sceneRuleRelService;

    @Override
    public void afterPropertiesSet() throws Exception {

        List<Scene> scenes = sceneService.list();

        SceneCache.put2map(scenes);

        List<ConcurrentMap<String, Map<String, RuleEntity>>> sceneRuleList = new ArrayList<>();

        for (Scene scene : scenes) {

            ConcurrentMap<String, Map<String, RuleEntity>> sceneRuleMap = new ConcurrentHashMap<>();

            Map<String, RuleEntity> ruleMap = new HashMap<>();

            List<SceneRuleRel> sceneRuleRels = sceneRuleRelService.list(new QueryWrapper<SceneRuleRel>().eq("scene_id", scene.getSceneId()).eq("status", "VALID"));

            SceneRuleRel sceneRuleRel = sceneRuleRels.stream().sorted(Comparator.comparing(SceneRuleRel::getPriority).reversed()).findFirst().get();

            RuleEntity ruleEntity = ruleService.getOne(new QueryWrapper<RuleEntity>().eq("rule_id", sceneRuleRel.getRuleId()));

            ruleMap.put(ruleEntity.getRuleId(), ruleEntity);

            sceneRuleMap.put(scene.getSceneId(), ruleMap);

            sceneRuleList.add(sceneRuleMap);
        }

        SceneRuleCache.put2map(sceneRuleList);
    }
}
