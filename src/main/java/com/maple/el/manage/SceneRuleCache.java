package com.maple.el.manage;

import com.maple.el.entity.RuleEntity;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author: maple
 * @version: RuleCache.java, v 0.1 2020年09月15日 15:43 maple Exp $
 */
public class SceneRuleCache {

    // 一个场景可能对应一个优先级别最高的规则
    // <<scene_id, Map<rule_id, Rule>>
    private static ConcurrentMap<String, Map<String, RuleEntity>> sceneRuleMap = new ConcurrentHashMap<>();

    /**
     * 把场景对应的规则缓存一下
     *
     * @param sceneRuleList
     */
    public static void put2map(List<ConcurrentMap<String, Map<String, RuleEntity>>> sceneRuleList) {

        if (!sceneRuleMap.isEmpty()) {
            sceneRuleMap.clear();
        }

        for (ConcurrentMap<String, Map<String, RuleEntity>> sceneRule : sceneRuleList) {

            sceneRuleMap.putAll(sceneRule);
        }
    }

    /**
     * 根据场景ID获取对应的规则
     *
     * @return
     */
    public static Map<String, RuleEntity> getRuleBySceneId(String sceneId) {
        return sceneRuleMap.get(sceneId);
    }

    public static ConcurrentMap<String, Map<String, RuleEntity>> getAll() {
        return sceneRuleMap;
    }
}
