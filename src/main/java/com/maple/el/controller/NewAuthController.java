package com.maple.el.controller;

import com.maple.el.newauth.AuthEnvelope;
import com.maple.el.newauth.RuleType;
import com.maple.el.newauth.parser.RuleParser;
import com.maple.el.newauth.request.DefaultAuthScene;
import com.maple.el.newauth.request.DefaultAuthTarget;
import com.maple.el.newauth.result.AuthCheckResult;
import com.maple.el.newauth.result.AuthExecuteResult;
import com.maple.el.service.NewAuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class NewAuthController {

    @Resource
    private NewAuthService newAuthService;

    @RequestMapping("/checkAuth")
    public boolean checkAuth(String ruleId, String eventCode, String productCode, String ipRoleType) {

        DefaultAuthScene authScene = new DefaultAuthScene();
        authScene.setEventCode(eventCode);
        authScene.setProductCode(productCode);

        DefaultAuthTarget authTarget = new DefaultAuthTarget();
        authTarget.setIpRoleType(ipRoleType);

        AuthEnvelope authEnvelope = new AuthEnvelope(authTarget, authScene);

        AuthExecuteResult authExecuteResult = newAuthService.newAuth(ruleId, authEnvelope);

        AuthCheckResult build = AuthCheckResult.builder().
                setGranted(authExecuteResult.isPassed())
                .setUnPassedRules(authExecuteResult.getUnPassedRules())
                .build();

        return build.isGranted();
    }
}
