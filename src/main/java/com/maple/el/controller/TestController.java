package com.maple.el.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
public class TestController {

    @RequestMapping("/test")
    public String test() {
        return "OK";
    }

    private static final ExecutorService service = Executors.newCachedThreadPool();

    @RequestMapping("/test1")
    public String test1() throws ExecutionException, InterruptedException {

        return null;
    }

    // 1、查询A表，只有10条数据，拿到 ids， 这一步查询 很快

    // 2、根据ids in 查询 表名表，因为id是索引，只 in 了 10 条数据，这一步会走索引，查询很快
    //  拿到 表名List（10条数据）

    // 匹配的问题：
    //第二步：循环表名List，放进map里，key是表名，值是DTO(你想放啥就放啥)，最后是：Map<String, DTO> map = new HashMap<>();


    // 3、根据表名List 拿到 对应的count（如果表很大，则考虑用多线程去count，最后获取每个线程的结果，汇总）
    //
    //Map<String, Integer> resultMap = new HashMap<>();表名，count



        /*boolean flag = true;

        //查询出来的表名list
        List<String> tableList = new ArrayList<>();

        // 表名，线程任务
        Map<String, Future> futureMap = new HashMap<>();

        // 表名，count
        Map<String, Integer> resultMap = new HashMap<>();

        for (int i = 0; i < tableList.size(); i++) {

            Future<Boolean> task = service.submit(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {

                    //selsct count(*) from table
                    return true;
                }
            });

            futureMap.put(tableList.get(i), task);
        }

        while (flag) {

            for (Map.Entry<String, Future> entry : futureMap.entrySet()) {

                if (resultMap.containsKey(entry.getKey())) {
                    continue;
                }

                if (entry.getValue().isDone()) {
                    Integer countObj = (Integer) entry.getValue().get();// 这个就是 线程返回的结果
                    resultMap.put(entry.getKey(), countObj);
                }

                flag = !(resultMap.size() == tableList.size());
            }
        }


    }*/

}
