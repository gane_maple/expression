package com.maple.el.controller;

import com.maple.el.entity.RuleEntity;
import com.maple.el.entity.Scene;
import com.maple.el.fact.AuthContextFact;
import com.maple.el.fact.AuthResult;
import com.maple.el.manage.AbstractAuthManage;
import com.maple.el.manage.SceneCache;
import com.maple.el.manage.SceneRuleCache;
import com.maple.el.service.AuthService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class AuthController {

    @Resource
    private AuthService authService;

    @Resource
    private AbstractAuthManage abstractAuthManage;

    @RequestMapping("/checkAuthOptimize")
    public AuthResult checkAuthOptimize(AuthContextFact authContextFact) {

        return authService.auth(authContextFact);
    }

    @RequestMapping("/refresh")
    public Map<String, RuleEntity> refresh() {

        try {
            abstractAuthManage.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Scene> sceneMap = SceneCache.getAll();
        System.out.println("刷新后的场景为：" + sceneMap);

        Map<String, RuleEntity> sceneRuleMap = SceneRuleCache.getRuleBySceneId("10002");
        System.out.println("刷新后的场景规则关系为：" + sceneRuleMap);

        return sceneRuleMap;
    }
}
