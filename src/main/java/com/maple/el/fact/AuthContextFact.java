package com.maple.el.fact;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: maple
 * @version: AuthContextFact.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class AuthContextFact implements Serializable {

    private String eventCode;

    private String productCode;
}
