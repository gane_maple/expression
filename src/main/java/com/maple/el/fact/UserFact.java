package com.maple.el.fact;

import lombok.Data;

/**
 * @author: maple
 * @version: UserFact.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class UserFact extends BaseFact {

    private boolean exists;

    private String status;

    private String age;

    private String nationality;
}
