package com.maple.el.fact;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: maple
 * @version: AccountFact.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class AccountFact extends BaseFact {

    private BigDecimal balance;

    private String status;

    private String accountType;
}
