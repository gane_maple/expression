package com.maple.el.fact;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: maple
 * @version: AuthResult.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class AuthResult implements Serializable {

    private boolean pass;

    private String reason;
}
