package com.maple.el.newauth;

import com.maple.el.newauth.loader.AbstractFactLoader;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: maple
 * @version: FactLoaderRegister.java, v 0.1 2020年09月19日 17:05 maple Exp $
 */
public class FactLoaderRegister {

    /**
     * 规则事实加载器存储
     */
    private static final Map<String, AbstractFactLoader<?, ?>> FACT_LOADER_STORAGE = new ConcurrentHashMap<>(10);

    /**
     * 规则事实加载器注册
     *
     * @param factLoader 规则事实加载器
     */
    public static void register(AbstractFactLoader<?, ?> factLoader) {
        try {
            List<Class> classes = new ArrayList<>();
            Class superClz = factLoader.getClass();
            while (superClz != null && !superClz.equals(Object.class)) {
                classes.add(superClz);

                superClz = superClz.getSuperclass();
            }

            Collections.reverse(classes);

            boolean contextTypeConfirm = false;
            Type factType = null;

            for (Class clz : classes) {
                if (clz == AbstractFactLoader.class) {
                    continue;
                }

                Type genericSuperclass = clz.getGenericSuperclass();
                ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

                if (contextTypeConfirm) {
                    if (actualTypeArguments[0] instanceof TypeVariable) {
                        continue;
                    }

                    factType = actualTypeArguments[0];
                } else {
                    if (actualTypeArguments[1] instanceof TypeVariable) {
                        if (actualTypeArguments[0] instanceof TypeVariable) {
                            continue;
                        }

                        contextTypeConfirm = true;
                        continue;
                    }

                    factType = actualTypeArguments[1];
                }

                break;
            }

            if (factType == null) {
                return;
            }

            Class<?> returnTypeClz = Class.forName(factType.getTypeName());

            /*RuleFact ruleFact = returnTypeClz.getAnnotation(RuleFact.class);
            String factKey;

            if (ruleFact == null || StringUtils.isEmpty(ruleFact.factKey())) {
                factKey = returnTypeClz.getSimpleName();
            } else {
                factKey = ruleFact.factKey();
        }*/

            FACT_LOADER_STORAGE.put(returnTypeClz.getSimpleName(), factLoader);

        } catch (Exception e) {
        }
    }

    /**
     * 根据factKey获取并校验规则事实加载器
     *
     * @param factKey factKey
     * @return 规则事实加载器
     */
    public static <C, F> AbstractFactLoader<C, F> getAndCheck(String factKey) {
        AbstractFactLoader<C, F> factLoader = (AbstractFactLoader<C, F>) FACT_LOADER_STORAGE.get(factKey);

        if (factLoader == null) {
            throw new IllegalStateException("Cannot found factLoader with factKey : " + factKey);
        }

        return factLoader;
    }
}
