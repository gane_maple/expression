package com.maple.el.newauth.request;

/**
 * @author: maple
 * @version: AbstractAuthTarget.java, v 0.1 2020年09月19日 17:42 maple Exp $
 */
public abstract class AbstractAuthTarget implements AuthTarget {

    /**
     * 限权对象ID
     */
    private String targetId;

    private String targetType;

    @Override
    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTargetType() {
        return targetType;
    }

    /**
     * Setter method for property <tt>targetType</tt>.
     *
     * @param targetType value to be assigned to property targetType
     */
    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}
