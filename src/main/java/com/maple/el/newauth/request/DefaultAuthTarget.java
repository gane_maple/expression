package com.maple.el.newauth.request;

import com.maple.el.newauth.request.AbstractAuthTarget;
import lombok.Data;

/**
 * @author: maple
 * @version: DefaultAuthTarget.java, v 0.1 2020年09月19日 17:44 maple Exp $
 */
@Data
public class DefaultAuthTarget extends AbstractAuthTarget {

    private String ipRoleType;

}
