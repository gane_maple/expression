package com.maple.el.newauth.request;

import com.maple.el.newauth.request.AbstractAuthScene;
import lombok.Data;

/**
 * @author: maple
 * @version: DefaultAuthScene.java, v 0.1 2020年09月19日 17:40 maple Exp $
 */
@Data
public class DefaultAuthScene extends AbstractAuthScene {

    /**
     * 事件码
     */
    private String eventCode;

    /**
     * 产品码
     */
    private String productCode;

    /**
     * 请求来源
     */
    private String requestSource;
}
