package com.maple.el.newauth.request;

/**
 * @author: maple
 * @version: AuthTarget.java, v 0.1 2020年09月19日 15:35 maple Exp $
 */
public interface AuthTarget {

    String getTargetId();
}
