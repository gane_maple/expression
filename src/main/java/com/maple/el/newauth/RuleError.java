/**
 * Alipay.com Inc. Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.maple.el.newauth;


public class RuleError {

    /**
     * 默认规则未通过时的错误码，当限权失败且失败规则未配置对应的错误码映射时，错误码默认映射为这个
     */
    public static final String DEFAULT_RULE_ERROR_CODE = "AE15112001527794";

    /**
     * 错误的规则ID
     */
    private String ruleId;

    /**
     * 错误的规则名称
     */
    private String ruleName;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 默认构造方法
     */
    public RuleError() {
    }

    /**
     * 默认构造器
     *
     * @param ruleId    规则ID
     * @param ruleName  规则名称
     * @param errorCode 错误码
     * @param errorMsg  错误描述
     */
    public RuleError(String ruleId, String ruleName, String errorCode, String errorMsg) {
        this.ruleId = ruleId;
        this.ruleName = ruleName;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    /**
     * 生成默认错误码
     *
     * @param ruleId   规则ID
     * @param ruleName 规则名称
     * @param errorMsg 错误描述
     */
    public static RuleError newDefaultError(String ruleId, String ruleName, String errorMsg) {
        return new RuleError(ruleId, ruleName, DEFAULT_RULE_ERROR_CODE, errorMsg);
    }

    /**
     * 生成错误码
     *
     * @param ruleId    规则ID
     * @param ruleName  规则名称
     * @param errorCode 错误码
     * @param errorMsg  错误描述
     */
    public static RuleError newError(String ruleId, String ruleName, String errorCode, String errorMsg) {
        return new RuleError(ruleId, ruleName, errorCode, errorMsg);
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}