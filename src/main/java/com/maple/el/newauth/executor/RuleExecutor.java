package com.maple.el.newauth.executor;

import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.Rule;

import java.util.Map;

/**
 * @author: maple
 * @version: RuleExecutor.java, v 0.1 2020年09月19日 12:57 maple Exp $
 */
public interface RuleExecutor {

    ExecuteResult execute(Rule rule, Map<String, Object> ruleFacts);
}
