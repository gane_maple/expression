package com.maple.el.newauth.executor;

import com.maple.el.newauth.result.AuthExecuteResult;
import com.maple.el.newauth.AuthRuleEvaluationContext;
import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.Rule;
import com.maple.el.newauth.manager.RuleManager;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpression;

import java.util.Map;

/**
 * @author: maple
 * @version: AuthRuleExecutor.java, v 0.1 2020年09月19日 12:59 maple Exp $
 */
public class AuthRuleExecutor implements RuleExecutor {

    /**
     * 子规则引用函数名
     */
    public static final String CHILD_RULE_EXECUTE_FUNCTION_NAME = "childRule";

    /**
     * 规则执行上下文引用占位符
     */
    public static final String EVALUATION_CONTEXT_VARIABLE_NAME = "evaluationContext";

    private static RuleManager ruleManager;

    public void setRuleManager(RuleManager ruleManager) {
        AuthRuleExecutor.ruleManager = ruleManager;
    }


    @Override
    public ExecuteResult execute(Rule rule, Map<String, Object> ruleFacts) {

        SpelExpression spelExpression = rule.getSpelExpression();

        //构造规则执行上下文
        AuthRuleEvaluationContext evaluationContext = new AuthRuleEvaluationContext(ruleFacts);

        Boolean isPassed;

        //规则执行
        try {
            isPassed = spelExpression.getValue(evaluationContext, Boolean.class);
        } catch (SpelEvaluationException e) {
            System.out.println("spel rule execute exception");
            throw e;
        }

        //封装规则执行最终结果
        return constructExecuteResult(isPassed, rule, evaluationContext);
    }

    public static Boolean childRule(String childRuleId, AuthRuleEvaluationContext evaluationContext) {
        //获取spelRule
        Rule rule = ruleManager.getRuleById(childRuleId);

        //子规则执行
        SpelExpression spelExpression = rule.getSpelExpression();
        Boolean isPassed = spelExpression.getValue(evaluationContext, Boolean.class);

        //如果子规则执行未通过，保存到执行结果中
        if (!isPassed) {
            evaluationContext.addUnPassedRule(rule);
        }

        return isPassed;
    }

    private AuthExecuteResult constructExecuteResult(Boolean isPassed, Rule rule, AuthRuleEvaluationContext evaluationContext) {
        if (!isPassed) {
            evaluationContext.addUnPassedRule(rule);
        }

        AuthExecuteResult executeResult = evaluationContext.getExecuteResult();

        executeResult.setPassed(isPassed);

        return executeResult;
    }
}
