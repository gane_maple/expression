package com.maple.el.newauth.executor;

import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.Rule;
import com.maple.el.newauth.executor.RuleExecutor;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.support.ReflectivePropertyAccessor;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Arrays;
import java.util.Map;

/**
 * @author: maple
 * @version: DefaultRuleExecutor.java, v 0.1 2020年09月19日 14:27 maple Exp $
 */
public class DefaultRuleExecutor implements RuleExecutor {

    /**
     * spel表达式MAP类型字段数据访问器
     */
    private static final PropertyAccessor MAP_ACCESSOR = new MapAccessor();

    /**
     * spel表达式任意类型字段访问器，通过反射实现
     */
    private static final PropertyAccessor REFLECT_ACCESSOR = new ReflectivePropertyAccessor();


    @Override
    public ExecuteResult execute(Rule rule, Map<String, Object> ruleFacts) {
        //获取SpelExpression
        SpelExpression spelExpression = rule.getSpelExpression();

        //构造规则执行上下文
        StandardEvaluationContext evaluationContext = new StandardEvaluationContext(ruleFacts);
        evaluationContext.setPropertyAccessors(Arrays.asList(MAP_ACCESSOR, REFLECT_ACCESSOR));

        //非空判定
        Object value;

        //规则执行
        try {
            value = spelExpression.getValue(evaluationContext);
        } catch (SpelEvaluationException e) {
            System.out.println("spel rule execute exception");
            throw e;
        }

        //封装规则执行最终结果
        return new ExecuteResult(value);
    }
}
