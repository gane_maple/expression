package com.maple.el.newauth.loader;

/**
 * @author: maple
 * @version: FactLoader.java, v 0.1 2020年09月19日 17:03 maple Exp $
 */
public interface FactLoader<C, F> {

    F load(C ruleContext);
}
