package com.maple.el.newauth.loader;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maple.el.dao.RuleDAO;
import com.maple.el.entity.RuleEntity;
import com.maple.el.newauth.loader.RuleLoader;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: maple
 * @version: DefaultRuleLoader.java, v 0.1 2020年09月19日 13:30 maple Exp $
 */
public class DefaultRuleLoader implements RuleLoader {

    @Resource
    private RuleDAO ruleDAO;

    @Override
    public RuleEntity getRuleById(String ruleId) {

        RuleEntity ruleEntity = ruleDAO.selectOne(new QueryWrapper<RuleEntity>().eq("rule_id", ruleId));

        return ruleEntity;
    }

    @Override
    public List<RuleEntity> queryAll() {

        return ruleDAO.selectList(null);
    }

}
