package com.maple.el.newauth.loader;

import com.maple.el.newauth.FactLoaderRegister;
import com.maple.el.newauth.loader.FactLoader;

/**
 * @author: maple
 * @version: AbstractFactLoader.java, v 0.1 2020年09月19日 17:04 maple Exp $
 */
public abstract class AbstractFactLoader<C, F> implements FactLoader<C, F> {

    /**
     * 生成一个抽象类默认构造器，使得所有子类自动注册到规则事实加载器注册中心
     */
    public AbstractFactLoader() {
        FactLoaderRegister.register(this);
    }
}
