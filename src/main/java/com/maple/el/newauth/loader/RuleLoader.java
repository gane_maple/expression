package com.maple.el.newauth.loader;

import com.maple.el.entity.RuleEntity;

import java.util.List;

/**
 * @author: maple
 * @version: RuleLoader.java, v 0.1 2020年09月19日 13:30 maple Exp $
 */
public interface RuleLoader {

    /**
     * 根据规则ID获取规则的数据模型
     *
     * @param ruleId 规则ID
     * @return 规则数据模型
     */
    RuleEntity getRuleById(String ruleId);

    /**
     * 获取所有规则实体
     *
     * @return 规则实体列表
     */
    List<RuleEntity> queryAll();
}
