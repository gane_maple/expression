package com.maple.el.newauth;

/**
 * @author: maple
 * @version: RuleNotExistsException.java, v 0.1 2020年09月19日 13:49 maple Exp $
 */
public class RuleNotExistsException extends RuntimeException {

    private final String ruleId;

    public RuleNotExistsException(String ruleId) {
        super("Cannot found rule by ruleId:" + ruleId);
        this.ruleId = ruleId;
    }

    public String getRuleId() {
        return ruleId;
    }
}
