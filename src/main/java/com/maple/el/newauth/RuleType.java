/**
 * Alipay.com Inc. Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.maple.el.newauth;

import java.util.Arrays;


public enum RuleType {

    /**
     * 路由规则
     */
    ROUTER_RULE,

    /**
     * 限权规则
     */
    AUTH_RULE,

    /**
     * 结果组装规则
     */
    RESULT_ASSEMBLE_RULE;

    public static RuleType getByCode(String code) {
        return Arrays.stream(RuleType.values()).filter((ruleType) -> ruleType.name().equals(code)).findFirst().orElse(null);
    }
}