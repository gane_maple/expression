package com.maple.el.newauth.parser;

import com.maple.el.newauth.executor.AuthRuleExecutor;
import com.maple.el.newauth.manager.RuleManager;
import com.maple.el.newauth.parser.AbstractRuleParser;
import org.springframework.expression.spel.ast.FunctionReference;
import org.springframework.expression.spel.ast.SpelNodeImpl;
import org.springframework.expression.spel.ast.StringLiteral;
import org.springframework.expression.spel.ast.VariableReference;

import java.util.Set;

/**
 * @author: maple
 * @version: AuthRuleParser.java, v 0.1 2020年09月19日 12:54 maple Exp $
 */
public class AuthRuleParser extends AbstractRuleParser {

    /**
     * 子规则引用表达式前缀
     */
    private static final String CHILD_RULE_EXPRESSION_FUNCTION_NAME_PREFIX = "#" + AuthRuleExecutor.CHILD_RULE_EXECUTE_FUNCTION_NAME + "(";

    /**
     * evaluationContext的变量引用表达式
     */
    private static final String EVALUATION_CONTEXT_VARIABLE_REFERENCE_EXPR = "#" + AuthRuleExecutor.EVALUATION_CONTEXT_VARIABLE_NAME;

    /**
     * spel结点的第一个子规则结点的index
     */
    public static final int SPEL_NODE_FIRST_CHILD_INDEX = 0;

    /**
     * spel结点的第二个子规则结点的index
     */
    public static final int SPEL_NODE_SECOND_CHILD_INDEX = 1;

    /**
     * 子规则引用表达式中方法参数数量
     */
    public static final int CHILD_RULE_EXPRESSION_FUNCTION_ARGS_COUNT = 2;

    /**
     * 规则管理器
     */
    private RuleManager ruleManager;

    public void setRuleManager(RuleManager ruleManager) {
        this.ruleManager = ruleManager;
    }

    @Override
    protected Set<String> getFactKeyIfOtherNode(SpelNodeImpl currentNode, SpelNodeImpl parentNode) {
        return getFactKeysIfChildRuleIdNode(currentNode, parentNode);
    }

    private Set<String> getFactKeysIfChildRuleIdNode(SpelNodeImpl currentNode, SpelNodeImpl parentNode) {
        //当前结点不是StringLiteral或则父结点不是FunctionReference时，直接返回null
        if (!(currentNode instanceof StringLiteral) || !(parentNode instanceof FunctionReference)) {
            return null;
        }

        FunctionReference childRuleReferenceNode = ((FunctionReference) parentNode);

        //判断是否为子规则ID结点
        if (childRuleReferenceNode.toStringAST().startsWith(CHILD_RULE_EXPRESSION_FUNCTION_NAME_PREFIX)
                && childRuleReferenceNode.getChildCount() == CHILD_RULE_EXPRESSION_FUNCTION_ARGS_COUNT
                && childRuleReferenceNode.getChild(SPEL_NODE_FIRST_CHILD_INDEX) == currentNode
                && childRuleReferenceNode.getChild(SPEL_NODE_SECOND_CHILD_INDEX) instanceof VariableReference
                && childRuleReferenceNode.getChild(SPEL_NODE_SECOND_CHILD_INDEX).toStringAST().equals(EVALUATION_CONTEXT_VARIABLE_REFERENCE_EXPR)) {

            String childRuleId = (String) ((StringLiteral) currentNode).getLiteralValue().getValue();

            return ruleManager.getRuleByIdFromDB(childRuleId).getFactKeys();
        }

        return null;
    }
}
