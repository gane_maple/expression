package com.maple.el.newauth.parser;

import com.maple.el.newauth.parser.AbstractRuleParser;
import org.springframework.expression.spel.ast.SpelNodeImpl;

import java.util.Set;

/**
 * @author: maple
 * @version: DefaultRuleParser.java, v 0.1 2020年09月19日 13:46 maple Exp $
 */
public class DefaultRuleParser extends AbstractRuleParser {
    @Override
    protected Set<String> getFactKeyIfOtherNode(SpelNodeImpl currentNode, SpelNodeImpl parentNode) {
        return null;
    }
}
