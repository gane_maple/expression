package com.maple.el.newauth.parser;

import com.maple.el.entity.RuleEntity;
import com.maple.el.newauth.Rule;

/**
 * @author: maple
 * @version: RuleParser.java, v 0.1 2020年09月19日 12:45 maple Exp $
 */
public interface RuleParser {

    Rule parse(RuleEntity ruleEntity);
}
