package com.maple.el.newauth.parser;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.maple.el.entity.RuleEntity;
import com.maple.el.newauth.Rule;
import com.maple.el.newauth.RuleType;
import com.maple.el.newauth.SpelAstWalker;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.ast.CompoundExpression;
import org.springframework.expression.spel.ast.PropertyOrFieldReference;
import org.springframework.expression.spel.ast.SpelNodeImpl;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: maple
 * @version: AbstractAuthRuleParser.java, v 0.1 2020年09月19日 12:50 maple Exp $
 */
public abstract class AbstractRuleParser implements RuleParser {

    /**
     * spel结点的第一个子规则结点的index
     */
    public static final int SPEL_NODE_FIRST_CHILD_INDEX = 0;

    /**
     * Spel表达式解析器
     */
    private static final SpelExpressionParser SPEL_EXPRESSION_PARSER;

    static {
        SpelParserConfiguration spelParserConfiguration = new SpelParserConfiguration(SpelCompilerMode.MIXED,
                AuthRuleParser.class.getClassLoader());

        SPEL_EXPRESSION_PARSER = new SpelExpressionParser(spelParserConfiguration);
    }

    @Override
    public Rule parse(RuleEntity ruleEntity) {

        String expressionText = ruleEntity.getExpression();

        //表达式解析
        SpelExpression spelExpression;

        try {
            spelExpression = SPEL_EXPRESSION_PARSER.parseRaw(expressionText);
        } catch (ParseException e) {
            System.out.println("Parse spel expression exception, ruleEntity = " + ruleEntity);
            //非法规则表达式
            throw e;
        }

        //查找表达式中使用到到factKeys
        Set<String> factKeys = getFactKeys(spelExpression);

        //领域模型组装
        return transToRule(ruleEntity, spelExpression, factKeys);
    }

    /**
     * 从Spel规则语法树中找出使用到factKeys
     *
     * @param spelExpression spel规则表达式
     * @return factKeys
     */
    private Set<String> getFactKeys(SpelExpression spelExpression) {
        Set<String> factKeys = new HashSet<>();

        SpelAstWalker.walk((SpelNodeImpl) spelExpression.getAST(), (currentNode, parentNode) -> {
            String factKeyIfPrimaryExpr = getFactKeyIfFactKeyNode(currentNode, parentNode);

            if (!StringUtils.isEmpty(factKeyIfPrimaryExpr)) {
                factKeys.add(factKeyIfPrimaryExpr);
                return;
            }

            Set<String> factKeyIfOtherNode = getFactKeyIfOtherNode(currentNode, parentNode);

            if (CollectionUtils.isNotEmpty(factKeyIfOtherNode)) {
                factKeys.addAll(factKeyIfOtherNode);
            }
        });

        return factKeys;
    }

    private String getFactKeyIfFactKeyNode(SpelNodeImpl currentNode, SpelNodeImpl parentNode) {
        //结点不是PropertyOrFieldReference，直接返回null
        if (!(currentNode instanceof PropertyOrFieldReference)) {
            return null;
        }

        //父节点不是CompoundExpression时，直接保存到factKeys并返回
        if (!(parentNode instanceof CompoundExpression)) {
            return currentNode.toStringAST();
        }

        //当前结点是父节点的第一个子结点时，保存到factKeys
        if (parentNode.getChild(SPEL_NODE_FIRST_CHILD_INDEX) == currentNode) {
            return currentNode.toStringAST();
        }

        return null;
    }

    protected abstract Set<String> getFactKeyIfOtherNode(SpelNodeImpl currentNode, SpelNodeImpl parentNode);

    private Rule transToRule(RuleEntity ruleEntity, SpelExpression spelExpression, Set<String> factKeys) {
        return Rule.builder()
                .spelExpression(spelExpression)
                .ruleId(ruleEntity.getRuleId())
                .ruleName(ruleEntity.getRuleName())
                .ruleType(RuleType.getByCode(ruleEntity.getRuleType()))
                .expression(ruleEntity.getExpression())
                .memo(ruleEntity.getMemo())
                .factKeys(factKeys)
                .build();
    }
}
