package com.maple.el.newauth;

import com.maple.el.entity.Scene;
import com.maple.el.newauth.request.AuthScene;
import com.maple.el.newauth.request.AuthTarget;
import com.maple.el.newauth.result.AuthCheckResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: maple
 * @version: AuthEnvelope.java, v 0.1 2020年09月19日 15:34 maple Exp $
 */
public class AuthEnvelope {

    /**
     * 鉴权对象
     */
    private AuthTarget authTarget;

    /**
     * 鉴权场景
     */
    private AuthScene authScene;

    /**
     * 业务信息
     */
    /*private AuthBiz authBiz;*/

    /**
     * 匹配的场景与鉴权规则信息
     */
    private Scene authSceneRoute;

    /**
     * 规则事实
     */
    private Map<String, Object> factCache = new HashMap<>();

    /**
     * 鉴权结果
     */
    private AuthCheckResult authCheckResult;

    /**
     * 默认构造器
     */
    public AuthEnvelope() {
    }

    /**
     * 构造器
     *
     * @param authTarget 限权对象信息
     * @param authScene  限权场景信息
     */
    public AuthEnvelope(AuthTarget authTarget, AuthScene authScene) {
        this.authTarget = authTarget;
        this.authScene = authScene;
    }

    /**
     * Getter method for property <tt>authTarget</tt>.
     *
     * @return property value of authTarget
     */
    public AuthTarget getAuthTarget() {
        return authTarget;
    }

    /**
     * Setter method for property <tt>authTarget</tt>.
     *
     * @param authTarget value to be assigned to property authTarget
     */
    public void setAuthTarget(AuthTarget authTarget) {
        this.authTarget = authTarget;
    }

    /**
     * Getter method for property <tt>authScene</tt>.
     *
     * @return property value of authScene
     */
    public AuthScene getAuthScene() {
        return authScene;
    }

    /**
     * Setter method for property <tt>authScene</tt>.
     *
     * @param authScene value to be assigned to property authScene
     */
    public void setAuthScene(AuthScene authScene) {
        this.authScene = authScene;
    }

    /**
     * Getter method for property <tt>authBiz</tt>.
     *
     * @return property value of authBiz
     */
    /*public AuthBiz getAuthBiz() {
        return authBiz;
    }

    *//**
     * Setter method for property <tt>authBiz</tt>.
     *
     * @param authBiz value to be assigned to property authBiz
     *//*
    public void setAuthBiz(AuthBiz authBiz) {
        this.authBiz = authBiz;
    }*/

    /**
     * Getter method for property <tt>authSceneRoute</tt>.
     *
     * @return property value of authSceneRoute
     */
    public Scene getAuthSceneRoute() {
        return authSceneRoute;
    }

    /**
     * Setter method for property <tt>authSceneRoute</tt>.
     *
     * @param authSceneRoute value to be assigned to property authSceneRoute
     */
    public void setAuthSceneRoute(Scene authSceneRoute) {
        this.authSceneRoute = authSceneRoute;
    }

    /**
     * Getter method for property <tt>factCache</tt>.
     *
     * @return property value of factCache
     */
    public Map<String, Object> getFactCache() {
        return factCache;
    }

    /**
     * Setter method for property <tt>factCache</tt>.
     *
     * @param factCache value to be assigned to property factCache
     */
    public void setFactCache(Map<String, Object> factCache) {
        this.factCache = factCache;
    }

    /**
     * Getter method for property <tt>authCheckResult</tt>.
     *
     * @return property value of authCheckResult
     */
    public AuthCheckResult getAuthCheckResult() {
        return authCheckResult;
    }

    /**
     * Setter method for property <tt>authCheckResult</tt>.
     *
     * @param authCheckResult value to be assigned to property authCheckResult
     */
    public void setAuthCheckResult(AuthCheckResult authCheckResult) {
        this.authCheckResult = authCheckResult;
    }
}
