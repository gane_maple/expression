package com.maple.el.newauth;

import com.maple.el.newauth.executor.AuthRuleExecutor;
import com.maple.el.newauth.result.AuthExecuteResult;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.spel.support.ReflectivePropertyAccessor;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author: maple
 * @version: AuthRuleEvaluationContext.java, v 0.1 2020年09月19日 13:03 maple Exp $
 */
public class AuthRuleEvaluationContext extends StandardEvaluationContext {

    /**
     * spel表达式MAP类型字段数据访问器
     */
    private static final PropertyAccessor MAP_ACCESSOR = new MapAccessor();

    /**
     * spel表达式任意类型字段访问器，通过反射实现
     */
    private static final PropertyAccessor REFLECT_ACCESSOR = new ReflectivePropertyAccessor();

    /**
     * 子规则执行函数
     */
    private static final Method CHILD_RULE_EXECUTE_METHOD;

    static {
        try {
            CHILD_RULE_EXECUTE_METHOD = AuthRuleExecutor.class.getMethod(AuthRuleExecutor.CHILD_RULE_EXECUTE_FUNCTION_NAME, String.class,
                    AuthRuleEvaluationContext.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("cannot found childRule execute function", e);
        }
    }

    private final AuthExecuteResult executeResult;

    public AuthRuleEvaluationContext(Map<String, Object> ruleFacts) {
        //初始化执行结果
        this.executeResult = new AuthExecuteResult();

        //设置字段访问器
        this.setPropertyAccessors(Arrays.asList(MAP_ACCESSOR, REFLECT_ACCESSOR));

        //设置ruleFacts为根元素
        this.setRootObject(ruleFacts);

        //设置子规则函数引用
        this.setVariable(AuthRuleExecutor.CHILD_RULE_EXECUTE_FUNCTION_NAME, CHILD_RULE_EXECUTE_METHOD);
        this.setVariable(AuthRuleExecutor.EVALUATION_CONTEXT_VARIABLE_NAME, this);
    }

    public void addUnPassedRule(Rule childRule) {
        List<Rule> unPassedRules = this.executeResult.getUnPassedRules();

        if (unPassedRules == null) {
            executeResult.setUnPassedRules(new ArrayList<>());
        }

        executeResult.getUnPassedRules().add(childRule);
    }

    /**
     * Getter method for property <tt>executeResult</tt>.
     *
     * @return property value of executeResult
     */
    public AuthExecuteResult getExecuteResult() {
        return executeResult;
    }
}
