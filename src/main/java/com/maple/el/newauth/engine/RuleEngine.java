package com.maple.el.newauth.engine;

import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.factsource.RuleFactSource;

/**
 * @author: maple
 * @version: RuleEngine.java, v 0.1 2020年09月19日 14:17 maple Exp $
 */
public interface RuleEngine {

    ExecuteResult execute(String ruleId, RuleFactSource factSource);
}
