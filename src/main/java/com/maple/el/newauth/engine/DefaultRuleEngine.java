package com.maple.el.newauth.engine;

import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.Rule;
import com.maple.el.newauth.RuleType;
import com.maple.el.newauth.engine.RuleEngine;
import com.maple.el.newauth.executor.RuleExecutor;
import com.maple.el.newauth.factsource.RuleFactSource;
import com.maple.el.newauth.manager.RuleManager;
import org.springframework.util.Assert;

import java.util.Map;

/**
 * @author: maple
 * @version: DefaultRuleEngine.java, v 0.1 2020年09月19日 14:17 maple Exp $
 */
public class DefaultRuleEngine implements RuleEngine {

    /**
     * 规则管理器
     */
    private RuleManager ruleManager;

    public void setRuleManager(RuleManager ruleManager) {
        this.ruleManager = ruleManager;
    }

    /**
     * 规则类型与规则执行器的映射表
     */
    private Map<RuleType, RuleExecutor> specificRuleExecutor;

    public void setSpecificRuleExecutor(Map<RuleType, RuleExecutor> specificRuleExecutor) {
        this.specificRuleExecutor = specificRuleExecutor;
    }

    /**
     * 默认规则执行器
     */
    private RuleExecutor defaultRuleExecutor;

    public void setDefaultRuleExecutor(RuleExecutor defaultRuleExecutor) {
        this.defaultRuleExecutor = defaultRuleExecutor;
    }

    @Override
    public ExecuteResult execute(String ruleId, RuleFactSource factSource) {

        //加载规则领域模型
        Rule rule = ruleManager.getRuleById(ruleId);

        //加载规则执行需要的facts
        Map<String, Object> ruleFacts;

        try {
            ruleFacts = factSource.getRuleFacts(rule.getFactKeys());
        } catch (Exception e) {
            System.out.println("factSource execute exception, [rule = " + rule + "]");
            throw e;
        }

        //对加载对facts进行参数校验，ruleFacts不为null, 且ruleFacts应与factKeys一致
        Assert.notNull(ruleFacts, String.format("ruleFacts cannot be null, [rule = %s]", rule));
        Assert.isTrue(ruleFacts.keySet().containsAll(rule.getFactKeys()), String.format("ruleFacts should contains all factKeys, [rule = %s, ruleFacts = %s]", rule, ruleFacts));

        //根据规则类型获得规则执行器
        RuleExecutor ruleExecutor = specificRuleExecutor.getOrDefault(rule.getRuleType(), defaultRuleExecutor);

        //规则执行并返回
        return ruleExecutor.execute(rule, ruleFacts);
    }
}
