package com.maple.el.newauth.factsource;

import java.util.Map;
import java.util.Set;

/**
 * @author: maple
 * @version: RuleFactSource.java, v 0.1 2020年09月19日 14:17 maple Exp $
 */
public interface RuleFactSource {

    /**
     * 获取规则执行需要的facts入参
     *
     * @param factKeys factKeys
     * @return 规则执行需要的所有facts
     */
    Map<String, Object> getRuleFacts(Set<String> factKeys);
}
