package com.maple.el.newauth.manager;

import com.maple.el.entity.RuleEntity;
import com.maple.el.newauth.Rule;
import com.maple.el.newauth.RuleNotExistsException;
import com.maple.el.newauth.RuleType;
import com.maple.el.newauth.loader.RuleLoader;
import com.maple.el.newauth.parser.RuleParser;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author: maple
 * @version: DefaultRuleManager.java, v 0.1 2020年09月19日 13:25 maple Exp $
 */
@Component
public class DefaultRuleManager implements RuleManager, ApplicationListener<ContextRefreshedEvent> {

    //规则本地缓存
    private static Map<String, Rule> LOCAL_RULE_CACHE_MAP = Collections.emptyMap();

    //规则数据加载器
    private RuleLoader ruleLoader;

    private Map<RuleType, RuleParser> specificRuleParser;

    private RuleParser defaultRuleParser;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initRuleCache();
    }

    //刷新规则缓存
    public void initRuleCache() {
        LOCAL_RULE_CACHE_MAP = ruleLoader.queryAll().stream().map(ruleEntity -> {
            try {
                return constructRule(ruleEntity);
            } catch (Exception e) {
                System.out.println("Exception happen when construct rule from rule entity : " + ruleEntity);
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toMap(Rule::getRuleId, rule -> rule));
    }

    @Override
    public Rule getRuleById(String ruleId) {
        Rule rule = LOCAL_RULE_CACHE_MAP.get(ruleId);

        //规则模型不为空否则抛出错误
        if (rule == null) {
            throw new RuleNotExistsException(ruleId);
        }

        return rule;
    }

    @Override
    public Rule getRuleByIdFromDB(String ruleId) {

        RuleEntity ruleEntity = ruleLoader.getRuleById(ruleId);

        if (ruleEntity == null) {
            throw new RuleNotExistsException(ruleId);
        }

        return constructRule(ruleEntity);
    }

    private Rule constructRule(RuleEntity ruleEntity) {
        //获取规则类型enum
        String ruleTypeCode = ruleEntity.getRuleType();
        RuleType ruleType = RuleType.getByCode(ruleTypeCode);

        //规则类型enum不为空否则抛出系统异常
        Assert.notNull(ruleType, "cannot get corresponding ruleType with code : " + ruleTypeCode + " [ruleEntity = " + ruleEntity + "]");

        //获取规则解析器
        RuleParser ruleParser = specificRuleParser.getOrDefault(ruleType, defaultRuleParser);

        //规则解析器不为空否则抛出系统异常
        Assert.notNull(ruleParser, "cannot get corresponding ruleParser with rule type: " + ruleType);

        //解析规则数据模型为对应的规则领域模型
        Rule rule = ruleParser.parse(ruleEntity);

        //解析后的规则不为空否则抛出系统异常
        Assert.notNull(rule, "parsed rule cannot be null" + " [ruleEntity = " + ruleEntity + "]");

        //领域模型自检
        rule.dataRequiredCheck();

        return rule;
    }

    public void setRuleLoader(RuleLoader ruleLoader) {
        this.ruleLoader = ruleLoader;
    }

    public void setSpecificRuleParser(Map<RuleType, RuleParser> specificRuleParser) {
        this.specificRuleParser = specificRuleParser;
    }

    public void setDefaultRuleParser(RuleParser defaultRuleParser) {
        this.defaultRuleParser = defaultRuleParser;
    }
}
