package com.maple.el.newauth.manager;

import com.maple.el.newauth.Rule;

/**
 * @author: maple
 * @version: RuleManager.java, v 0.1 2020年09月19日 13:00 maple Exp $
 */
public interface RuleManager {

    Rule getRuleById(String ruleId);

    Rule getRuleByIdFromDB(String ruleId);
}
