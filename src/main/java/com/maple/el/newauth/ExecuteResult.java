package com.maple.el.newauth;

import java.io.Serializable;

/**
 * @author: maple
 * @version: ExecuteResult.java, v 0.1 2020年09月19日 12:55 maple Exp $
 */
public class ExecuteResult implements Serializable {

    /**
     * 规则执行结果
     */
    private Object value;

    public ExecuteResult(Object value) {
        this.value = value;
    }

    public ExecuteResult() {
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
