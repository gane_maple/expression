package com.maple.el.newauth.fact;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: maple
 * @version: NewAccountFact.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class NewAccountFact {

    private BigDecimal balance;

    private String status;

    private String accountType;
}
