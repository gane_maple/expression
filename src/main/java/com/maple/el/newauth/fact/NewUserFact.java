package com.maple.el.newauth.fact;

import lombok.Data;

/**
 * @author: maple
 * @version: UserFact.java, v 0.1 2020年09月12日 18:18 maple Exp $
 */
@Data
public class NewUserFact {

    private boolean exists;

    private String status;

    private Integer age;

    private String nationality;
}
