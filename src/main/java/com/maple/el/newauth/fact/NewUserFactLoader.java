package com.maple.el.newauth.fact;

import com.maple.el.newauth.AuthEnvelope;
import com.maple.el.newauth.loader.AbstractFactLoader;
import org.springframework.stereotype.Component;

/**
 * @author: maple
 * @version: NewUserFactLoader.java, v 0.1 2020年09月19日 17:04 maple Exp $
 */
@Component
public class NewUserFactLoader extends AbstractFactLoader<AuthEnvelope, NewUserFact> {

    @Override
    public NewUserFact load(AuthEnvelope ruleContext) {

        String targetId = ruleContext.getAuthTarget().getTargetId();
        // 通过 targetId 查询 user表
        // 这里写死
        NewUserFact userFact = new NewUserFact();

        userFact.setExists(false);
        userFact.setStatus("ENABLE");
        userFact.setAge(18);

        return userFact;
    }
}
