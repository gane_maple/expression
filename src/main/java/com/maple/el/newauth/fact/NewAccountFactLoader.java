package com.maple.el.newauth.fact;

import com.maple.el.newauth.AuthEnvelope;
import com.maple.el.newauth.loader.AbstractFactLoader;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author: maple
 * @version: AccountFactLoader.java, v 0.1 2020年09月19日 17:04 maple Exp $
 */
@Component
public class NewAccountFactLoader extends AbstractFactLoader<AuthEnvelope, NewAccountFact> {

    @Override
    public NewAccountFact load(AuthEnvelope ruleContext) {

        String targetId = ruleContext.getAuthTarget().getTargetId();

        // 通过 targetId 或者其他字段 查询 account 表
        // 这里写死
        NewAccountFact accountFact = new NewAccountFact();

        accountFact.setBalance(new BigDecimal("99"));
        accountFact.setStatus("ENABLE");

        return accountFact;
    }
}
