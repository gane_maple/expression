package com.maple.el.newauth;

import org.springframework.expression.spel.ast.SpelNodeImpl;

/**
 * @author: maple
 * @version: SpelAstWalker.java, v 0.1 2020年09月19日 13:16 maple Exp $
 */
public class SpelAstWalker {

    public interface Processor {

        /**
         * 对语法树结点进行处理
         *
         * @param currentNode 语法树结点
         * @param parentNode  当前语法树结点的父结点
         */
        void process(SpelNodeImpl currentNode, SpelNodeImpl parentNode);
    }

    public static void walk(SpelNodeImpl ast, Processor processor) {
        walk(ast, null, processor);
    }

    private static void walk(SpelNodeImpl currentNode, SpelNodeImpl parentNode, Processor processor) {

        processor.process(currentNode, parentNode);

        for (int i = 0; i < currentNode.getChildCount(); i++) {
            walk((SpelNodeImpl) currentNode.getChild(i), currentNode, processor);
        }
    }


}
