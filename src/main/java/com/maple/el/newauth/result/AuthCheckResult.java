package com.maple.el.newauth.result;

import com.maple.el.newauth.Rule;
import com.maple.el.newauth.RuleError;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: maple
 * @version: AuthCheckResult.java, v 0.1 2020年09月19日 16:50 maple Exp $
 */
public class AuthCheckResult {

    /**
     * 是否鉴权通过
     */
    private boolean granted;

    /**
     * 规则执行不通过的规则列表
     */
    private List<Rule> unPassedRules;

    /**
     * 未通过的规则对应的ERROR信息
     */
    private List<RuleError> ruleErrors;

    private AuthCheckResult(Builder builder) {
        this.granted = builder.granted;
        this.ruleErrors = builder.ruleErrors;
        this.unPassedRules = builder.unPassedRules;
    }

    /**
     * Getter method for property <tt>granted</tt>.
     *
     * @return property value of granted
     */
    public boolean isGranted() {
        return granted;
    }

    /**
     * Setter method for property <tt>granted</tt>.
     *
     * @param granted value to be assigned to property granted
     */
    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    /**
     * Getter method for property <tt>unPassedRules</tt>.
     *
     * @return property value of unPassedRules
     */
    public List<Rule> getUnPassedRules() {
        return unPassedRules;
    }

    /**
     * Setter method for property <tt>unPassedRules</tt>.
     *
     * @param unPassedRules value to be assigned to property unPassedRules
     */
    public void setUnPassedRules(List<Rule> unPassedRules) {
        this.unPassedRules = unPassedRules;
    }

    /**
     * Getter method for property <tt>ruleErrors</tt>.
     *
     * @return property value of ruleErrors
     */
    public List<RuleError> getRuleErrors() {
        return ruleErrors;
    }

    /**
     * Setter method for property <tt>ruleErrors</tt>.
     *
     * @param ruleErrors value to be assigned to property ruleErrors
     */
    public void setRuleErrors(List<RuleError> ruleErrors) {
        this.ruleErrors = ruleErrors;
    }

    /**
     * 向堆栈中添加错误对象。
     *
     * @param error
     */
    public void addError(RuleError error) {

        if (ruleErrors == null) {

            ruleErrors = new ArrayList<>();
        }
        ruleErrors.add(error);
    }

    /**
     * 构造器模式
     */
    public static class Builder {
        /**
         * 是否鉴权通过
         */
        private boolean granted;

        /**
         * 未通过的规则对应的ERROR信息
         */
        private List<RuleError> ruleErrors;

        /**
         * 规则执行不通过的规则列表
         */
        private List<Rule> unPassedRules;

        public AuthCheckResult build() {
            return new AuthCheckResult(this);
        }

        /**
         * Setter method for property <tt>granted</tt>.
         *
         * @param granted value to be assigned to property granted
         */
        public Builder setGranted(boolean granted) {
            this.granted = granted;
            return this;
        }

        /**
         * Setter method for property <tt>ruleErrors</tt>.
         *
         * @param ruleErrors value to be assigned to property ruleErrors
         */
        public void setRuleErrors(List<RuleError> ruleErrors) {
            this.ruleErrors = ruleErrors;
        }

        /**
         * Setter method for property <tt>unPassedRules</tt>.
         *
         * @param unPassedRules value to be assigned to property unPassedRules
         */
        public Builder setUnPassedRules(List<Rule> unPassedRules) {
            this.unPassedRules = unPassedRules;
            return this;
        }
    }

    public static Builder builder() {
        return new Builder();
    }
}
