package com.maple.el.newauth.result;

import com.maple.el.newauth.ExecuteResult;
import com.maple.el.newauth.Rule;

import java.util.List;

/**
 * @author: maple
 * @version: AuthExecuteResult.java, v 0.1 2020年09月19日 13:04 maple Exp $
 */
public class AuthExecuteResult extends ExecuteResult {

    /**
     * 规则执行是否通过
     */
    private boolean passed;

    /**
     * 规则执行不通过的规则列表
     */
    private List<Rule> unPassedRules;

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
        this.setValue(passed);
    }

    public List<Rule> getUnPassedRules() {
        return unPassedRules;
    }

    public void setUnPassedRules(List<Rule> unPassedRules) {
        this.unPassedRules = unPassedRules;
    }
}
