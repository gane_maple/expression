package com.maple.el.newauth;

import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Set;

/**
 * @author: maple
 * @version: AuthRule.java, v 0.1 2020年09月19日 12:45 maple Exp $
 */
public class Rule {

    public static class RuleBuilder {

        private String ruleId;

        private String ruleName;

        private RuleType ruleType;

        private String expression;

        private String memo;

        private Set<String> factKeys;

        private SpelExpression spelExpression;

        protected RuleBuilder() {
        }

        public RuleBuilder ruleId(String ruleId) {
            this.ruleId = ruleId;
            return this;
        }

        public RuleBuilder ruleName(String ruleName) {
            this.ruleName = ruleName;
            return this;
        }

        public RuleBuilder ruleType(RuleType ruleType) {
            this.ruleType = ruleType;
            return this;
        }

        public RuleBuilder expression(String expression) {
            this.expression = expression;
            return this;
        }

        public RuleBuilder memo(String memo) {
            this.memo = memo;
            return this;
        }

        public RuleBuilder factKeys(Set<String> factKeys) {
            this.factKeys = factKeys;
            return this;
        }

        public RuleBuilder spelExpression(SpelExpression spelExpression) {
            this.spelExpression = spelExpression;
            return this;
        }

        public Rule build() {
            return new Rule(this);
        }
    }

    private final String ruleId;

    private final String ruleName;

    private final RuleType ruleType;

    private final String expression;

    private final String memo;

    private final Set<String> factKeys;

    private final SpelExpression spelExpression;

    public String getRuleId() {
        return ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public String getExpression() {
        return expression;
    }

    public String getMemo() {
        return memo;
    }

    public Set<String> getFactKeys() {
        return factKeys;
    }

    public SpelExpression getSpelExpression() {
        return spelExpression;
    }

    protected Rule(RuleBuilder builder) {
        this.ruleId = builder.ruleId;
        this.ruleName = builder.ruleName;
        this.ruleType = builder.ruleType;
        this.expression = builder.expression;
        this.memo = builder.memo;
        this.spelExpression = builder.spelExpression;

        Assert.notNull(builder.factKeys, "factKeys cannot be null");
        this.factKeys = builder.factKeys == null ? Collections.emptySet() : Collections.unmodifiableSet(builder.factKeys);
    }

    public static RuleBuilder builder() {
        return new RuleBuilder();
    }

    public void dataRequiredCheck() {
        notBlank(ruleId, "ruleId is blank");

        notNull(ruleType, "ruleType is null");

        notBlank(ruleName, "ruleName is blank");

        notBlank(expression, "expression is blank");

        notNull(spelExpression, "spelExpression is null");

        notNull(factKeys, "factKeys is null");
    }

    protected static void notNull(Object target, String description) {
        if (null == target) {
            throw new IllegalArgumentException(description);
        }
    }

    /**
     * 判断目标字符串非空字符串，否则抛出IllegalArgumentException异常
     *
     * @param target      目标
     * @param description 为空时抛出的描述信息
     */
    protected static void notBlank(String target, String description) {
        if (StringUtils.isEmpty(target)) {
            throw new IllegalArgumentException(description);
        }
    }

}
