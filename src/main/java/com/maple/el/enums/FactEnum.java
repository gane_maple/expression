package com.maple.el.enums;

/**
 * @author: maple
 * @version: FactEnum.java, v 0.1 2020年09月15日 18:32 maple Exp $
 */
public enum FactEnum {

    USER_FACT("USER_FACT", "用户相关的鉴权"),
    ACCOUNT_FACT("ACCOUNT_FACT", "账户相关的鉴权");


    private final String code;

    private final String desc;

    FactEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }
}
