package com.maple.el.contant;

/**
 * @author: maple
 * @version: Constants.java, v 0.1 2020年09月15日 18:47 maple Exp $
 */
public interface Constants {

    String FACT = "Fact";

    String WELL = "#";

    String POINT = ".";

    String UNDERLINE = "_";

    String AUTH_CONTEXT_FACT = "AuthContextFact";
}
