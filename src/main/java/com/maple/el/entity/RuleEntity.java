package com.maple.el.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: maple
 * @version: Scene.java, v 0.1 2020年09月12日 18:02 mapleRule Exp $
 */
@Data
@TableName("rule")
public class RuleEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("rule_id")
    private String ruleId;

    @TableField("rule_name")
    private String ruleName;

    private String expression;

    @TableField("rule_type")
    private String ruleType;

    private String memo;
}
