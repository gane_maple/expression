package com.maple.el.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: maple
 * @version: Scene.java, v 0.1 2020年09月12日 18:02 maple Exp $
 */
@Data
@TableName("scene")
public class Scene implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("scene_id")
    private String sceneId;

    @TableField("scene_name")
    private String sceneName;

    private String expression;

    private String memo;
}
