package com.maple.el.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: maple
 * @version: Scene.java, v 0.1 2020年09月12日 18:02 mapleRule Exp $
 */
@Data
@TableName("scene_rule_rel")
public class SceneRuleRel implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("scene_rule_rel")
    private String sceneRuleRel;

    @TableField("scene_id")
    private String sceneId;

    @TableField("rule_id")
    private String ruleId;

    private Integer priority;

    private String status;
}
