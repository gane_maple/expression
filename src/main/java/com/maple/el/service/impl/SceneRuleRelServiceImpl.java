package com.maple.el.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maple.el.dao.SceneRuleRelDAO;
import com.maple.el.entity.SceneRuleRel;
import com.maple.el.service.SceneRuleRelService;
import org.springframework.stereotype.Service;

/**
 * @Description SceneRuleRelServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class SceneRuleRelServiceImpl extends ServiceImpl<SceneRuleRelDAO, SceneRuleRel> implements SceneRuleRelService {
}
