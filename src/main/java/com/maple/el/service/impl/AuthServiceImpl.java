package com.maple.el.service.impl;

import com.maple.el.contant.Constants;
import com.maple.el.entity.RuleEntity;
import com.maple.el.entity.Scene;
import com.maple.el.fact.AuthContextFact;
import com.maple.el.fact.AuthResult;
import com.maple.el.fact.BaseFact;
import com.maple.el.factloader.AbstractFactLoader;
import com.maple.el.factloader.FactLoaderFactory;
import com.maple.el.manage.SceneCache;
import com.maple.el.manage.SceneRuleCache;
import com.maple.el.service.AuthService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: maple
 * @version: AuthServiceImpl.java, v 0.1 2020年09月15日 17:29 maple Exp $
 */
@Service
public class AuthServiceImpl implements AuthService {

    @Override
    public AuthResult auth(AuthContextFact request) {

        Scene scene = matchScene(request);

        String expression = getRuleExpression(scene);

        BaseFact baseFact = getFact(expression);

        AuthResult authResult = matchRule(expression, baseFact);

        return authResult;
    }

    private Scene matchScene(AuthContextFact authContextFact) {

        Map<String, Scene> sceneMap = SceneCache.getAll();

        for (Map.Entry<String, Scene> sceneEntry : sceneMap.entrySet()) {

            EvaluationContext context = new StandardEvaluationContext();
            context.setVariable(Constants.AUTH_CONTEXT_FACT, authContextFact);

            ExpressionParser parser = new SpelExpressionParser();

            if (parser.parseExpression(sceneEntry.getKey()).getValue(context, Boolean.class)) {
                return sceneEntry.getValue();
            }
        }

        return null;
    }

    private String getRuleExpression(Scene scene) {

        Map<String, RuleEntity> ruleMap = SceneRuleCache.getRuleBySceneId(scene.getSceneId());

        for (Map.Entry<String, RuleEntity> entry : ruleMap.entrySet()) {

            return entry.getValue().getExpression();
        }

        return null;
    }

    private BaseFact getFact(String expression) {

        AbstractFactLoader factLoader = FactLoaderFactory.getFactLoader(substringFactUnderline(expression));

        return factLoader.factLoader();
    }

    private AuthResult matchRule(String expression, BaseFact baseFact) {

        AuthResult result = new AuthResult();

        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable(substringFact(expression), baseFact);

        ExpressionParser parser = new SpelExpressionParser();

        Boolean pass = parser.parseExpression(expression).getValue(context, Boolean.class);

        result.setPass(pass);
        result.setReason(pass ? "" : "用户不存在或状态不可用");

        return result;
    }

    /**
     * 从 #UserFact.exists == true and #UserFact.status == "ENABLE" 中 拿到 UserFact
     *
     * @param expression
     * @return
     */
    private String substringFact(String expression) {

        if (expression.indexOf(Constants.WELL) < 0) {
            return null;
        }

        String baseFact = StringUtils.substring(expression, expression.indexOf(Constants.WELL) + 1, expression.indexOf(Constants.POINT));

        if (!baseFact.endsWith(Constants.FACT)) {
            return null;
        }

        return baseFact;
    }

    /**
     * 从 #UserFact.exists == true and #UserFact.status == "ENABLE" 中 拿到 USER_FACT
     */
    private String substringFactUnderline(String expression) {

        //UserFact
        String fact = substringFact(expression);

        fact = fact.toUpperCase();

        return StringUtils.substring(fact, 0, fact.indexOf(Constants.FACT.toUpperCase())) + Constants.UNDERLINE + Constants.FACT.toUpperCase();
    }
}
