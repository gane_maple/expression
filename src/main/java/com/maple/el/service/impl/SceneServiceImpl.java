package com.maple.el.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maple.el.dao.SceneDAO;
import com.maple.el.entity.Scene;
import com.maple.el.service.SceneService;
import org.springframework.stereotype.Service;

/**
 * @Description RuleServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class SceneServiceImpl extends ServiceImpl<SceneDAO, Scene> implements SceneService {
}
