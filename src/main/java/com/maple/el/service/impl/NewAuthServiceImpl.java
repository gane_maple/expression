package com.maple.el.service.impl;

import com.maple.el.newauth.*;
import com.maple.el.newauth.engine.RuleEngine;
import com.maple.el.newauth.factsource.RuleFactSource;
import com.maple.el.newauth.loader.AbstractFactLoader;
import com.maple.el.newauth.result.AuthExecuteResult;
import com.maple.el.service.NewAuthService;
import org.springframework.stereotype.Component;

/**
 * @author: maple
 * @version: NewAuthServiceiMPL.java, v 0.1 2020年09月19日 17:26 maple Exp $
 */
@Component
public class NewAuthServiceImpl implements NewAuthService {

    private RuleEngine ruleEngine;

    public void setRuleEngine(RuleEngine ruleEngine) {
        this.ruleEngine = ruleEngine;
    }

    @Override
    public AuthExecuteResult newAuth(String ruleId, AuthEnvelope authEnvelope) {

        RuleFactSource ruleFactSource = (factKeys) -> {
            factKeys.forEach(factKey -> authEnvelope.getFactCache().computeIfAbsent(factKey, (key) -> {
                AbstractFactLoader<AuthEnvelope, ?> factLoader = FactLoaderRegister.getAndCheck(factKey);

                return factLoader.load(authEnvelope);
            }));

            return authEnvelope.getFactCache();
        };

        return (AuthExecuteResult) ruleEngine.execute(ruleId, ruleFactSource);
    }
}
