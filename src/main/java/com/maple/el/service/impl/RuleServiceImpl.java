package com.maple.el.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maple.el.dao.RuleDAO;
import com.maple.el.entity.RuleEntity;
import com.maple.el.service.RuleService;
import org.springframework.stereotype.Service;

/**
 * @Description RuleServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class RuleServiceImpl extends ServiceImpl<RuleDAO, RuleEntity> implements RuleService {
}
