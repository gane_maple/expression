package com.maple.el.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maple.el.entity.Scene;

/**
 * @Description SceneService
 * @Date 2020/4/24 7:40
 * @Created by 王弘博
 */
public interface SceneService extends IService<Scene> {
}
