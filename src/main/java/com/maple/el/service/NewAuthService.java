package com.maple.el.service;

import com.maple.el.newauth.AuthEnvelope;
import com.maple.el.newauth.result.AuthExecuteResult;

/**
 * @author: maple
 * @version: NewAuthService.java, v 0.1 2020年09月19日 17:24 maple Exp $
 */
public interface NewAuthService {

    AuthExecuteResult newAuth(String ruleId, AuthEnvelope authEnvelope);
}
