package com.maple.el.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maple.el.entity.RuleEntity;

/**
 * @Description DepartmentService
 * @Date 2020/4/24 7:40
 * @Created by 王弘博
 */
public interface RuleService extends IService<RuleEntity> {
}
