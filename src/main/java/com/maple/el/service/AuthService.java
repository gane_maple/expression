package com.maple.el.service;

import com.maple.el.fact.AuthContextFact;
import com.maple.el.fact.AuthResult;

/**
 * @author: maple
 * @version: AuthService.java, v 0.1 2020年09月15日 17:27 maple Exp $
 */
public interface AuthService {

    AuthResult auth(AuthContextFact request);
}
