package com.maple.el;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@MapperScan("com.maple.el.dao")
@SpringBootApplication
@ImportResource({"classpath:core-aprule.xml"})
public class ExpressionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpressionApplication.class, args);
    }

}
