package com.maple.el.factloader;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: maple
 * @version: FactFactory.java, v 0.1 2020年09月15日 17:37 maple Exp $
 */
public final class FactLoaderFactory {

    private static Map<String, AbstractFactLoader> factLoaders = new ConcurrentHashMap<>();

    public static void register(String key, AbstractFactLoader factLoader) {
        if (factLoaders.containsKey(key)) {
            return;
        }
        factLoaders.put(key, factLoader);
    }

    public static AbstractFactLoader getFactLoader(String key) {

        if (!factLoaders.containsKey(key)) {
            throw new RuntimeException(key + " not found.");
        }
        return factLoaders.get(key);
    }
}
