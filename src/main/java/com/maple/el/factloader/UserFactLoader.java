package com.maple.el.factloader;

import com.maple.el.enums.FactEnum;
import com.maple.el.fact.BaseFact;
import com.maple.el.fact.UserFact;
import org.springframework.stereotype.Component;

/**
 * @author: maple
 * @version: UserFactLoader.java, v 0.1 2020年09月15日 17:58 maple Exp $
 */
@Component
public class UserFactLoader extends AbstractFactLoader {

    @Override
    protected String getKey() {
        return FactEnum.USER_FACT.getCode();
    }

    /**
     * 获取数据且赋值，这里可能是查询DB，也可能是调第三方的接口
     *
     * @return
     */
    @Override
    public BaseFact factLoader() {

        UserFact userFact = new UserFact();

        userFact.setExists(true);
        userFact.setStatus("ENABLE");
        userFact.setAge("18");

        return userFact;
    }
}
