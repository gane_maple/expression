package com.maple.el.factloader;

import com.maple.el.fact.BaseFact;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author: maple
 * @version: AbstractFactLoader.java, v 0.1 2020年09月15日 18:00 maple Exp $
 */
public abstract class AbstractFactLoader<T> implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        FactLoaderFactory.register(getKey(), this);
    }

    protected abstract String getKey();

    public abstract BaseFact factLoader();
}
