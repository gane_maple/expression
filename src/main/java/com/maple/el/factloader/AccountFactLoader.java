package com.maple.el.factloader;

import com.maple.el.enums.FactEnum;
import com.maple.el.fact.AccountFact;
import com.maple.el.fact.BaseFact;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author: maple
 * @version: AccountFactLoader.java, v 0.1 2020年09月15日 17:58 maple Exp $
 */
@Component
public class AccountFactLoader extends AbstractFactLoader {

    @Override
    protected String getKey() {
        return FactEnum.ACCOUNT_FACT.getCode();
    }

    /**
     * 获取数据且赋值，这里可能是查询DB，也可能是调第三方的接口
     *
     * @return
     */
    @Override
    public BaseFact factLoader() {

        AccountFact accountFact = new AccountFact();

        accountFact.setBalance(new BigDecimal("101"));
        accountFact.setStatus("ENABLE");
        accountFact.setAccountType("Alipay");

        return accountFact;
    }
}
