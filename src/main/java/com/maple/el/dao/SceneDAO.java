package com.maple.el.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maple.el.entity.Scene;

/**
 * @Description SceneDAO
 * @Date 2020/4/24 7:39
 * @Created by 王弘博
 */
public interface SceneDAO extends BaseMapper<Scene> {
}
